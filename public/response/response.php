<?php

namespace Web\response;

class response
{
    public static function setStatusCode(int $code)
    {
        http_response_code($code);
        echo "Not Found";
    }
}