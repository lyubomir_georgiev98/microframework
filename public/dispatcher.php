<?php
namespace Web;

use Web\config\routeCollection;

class dispatcher implements dispatcherInterface
{
    public function routes(string $url, string $method): array
    {
        return routeCollection::createdUrls()->dispatch($method, $url);

    }
}