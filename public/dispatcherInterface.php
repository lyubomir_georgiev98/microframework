<?php
namespace Web;
interface dispatcherInterface
{
    public function routes(string $url, string $method);
}