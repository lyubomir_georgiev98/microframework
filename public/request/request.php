<?php

namespace Web\request;

use Web\dispatcher;

class request
{

    private array $server;

    public function __construct(array $server = [])
    {

        $this->server = $server;
    }
    //talk about problem with the read of the array
    public static function createFromGlobals(): self
    {
        return new self($_SERVER);
    }

    public static function server(string $key)
    {

        return $_SERVER[$key]?? null;
    }
}