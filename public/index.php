<?php

use Web\app\application;
use Web\dispatcher;


require_once __DIR__.'/../vendor/autoload.php';

//app must depend on dispatcher
//dependency injection for dispatcher
//implement singleton design pattern for app object

//container
//how to rename and change in other files



$containerBuilder = new \DI\ContainerBuilder();
$containerBuilder->useAnnotations(true);
$containerBuilder->addDefinitions(__DIR__.'/config/config.php');
$container = $containerBuilder->build();
$dispatcher = $container->get('dispatcher');


$app = application::getInstance($dispatcher);
$app->run();

