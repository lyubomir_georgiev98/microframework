<?php

use Web\dispatcher;
use function DI\create;
echo "hello this is config.php";
return [
    'dispatcher' => create(dispatcher::class),
    'routeCollection'=> create(\Web\config\routeCollection::class)
];