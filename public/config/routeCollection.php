<?php
namespace Web\config;
use FastRoute\RouteCollector;
use Struct\Controllers\homeController;
use Web\repositories\DogRepository;
use function FastRoute\simpleDispatcher;

class routeCollection
{
    public static function createdUrls(): \FastRoute\Dispatcher
    {
        return simpleDispatcher(function(RouteCollector $r) {

            $r->addRoute('GET', '/', function (    ){
                echo "anonymous function is called";
            });
            $r->addRoute('GET', '/index', [new homeController(new DogRepository()), 'index']);

            $r->addRoute('GET', '/create', [homeController::class, 'create']);
        });
    }

}
