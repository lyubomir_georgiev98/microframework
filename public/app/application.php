<?php

namespace Web\app;

use DI\Container;
use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use Web\dispatcherInterface;
use Web\request\request;
use Web\response\response;

require_once __DIR__.'/../../vendor/autoload.php';

class application
{

    private static application $instance ;
    private dispatcherInterface $dispatcher;

    private function __construct(dispatcherInterface $dispatcher)
    {
        $this->dispatcher=$dispatcher;
    }
    public static function getInstance($dispatchers): application
    {
        if(!isset(self::$instance)){
            self::$instance = new application($dispatchers);
        }
        return self::$instance;
    }

    public function run()
    {
        $uri= request::server('REQUEST_URI');
        $httpMethod = request::server('REQUEST_METHOD');


        $routeInfo = $this->dispatcher->routes($uri, $httpMethod);
        $container =new Container();
        switch ($routeInfo[0]) {
            case Dispatcher::NOT_FOUND:
                $array = [0=>"Web\\response\\response",
                    1=>"setStatusCode"];
               call_user_func($array,404);
                break;
            case Dispatcher::METHOD_NOT_ALLOWED:
                //$allowedMethods = $routeInfo[1];
                // ... 405 Method Not Allowed
                break;
            case Dispatcher::FOUND:

                $vars = $routeInfo[2];
                $handler = $routeInfo[1];
                echo "<pre>";
                var_dump((array)$handler[0]);
                echo "</pre>";
                $arrayHandler = [0=>"Struct\Controllers\homeController",
                    1=>"test"];
                call_user_func_array($handler,$vars);
                // ... call $handler with $vars
                break;
        }

    }
}