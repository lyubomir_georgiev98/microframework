<?php
namespace Struct\Controllers;
use Web\baseController\baseController;
use Web\repositories\DogRepository;

class homeController extends baseController
{
    private DogRepository $dogRepository;

    public function __construct(DogRepository $dogRepository)
    {
        $this->dogRepository=$dogRepository;
    }

    public function index(array $args = [])
    {
        $dogs = $this->dogRepository->getAll();
        parent::render();

    }
    public static function create(array $args)
    {
        parent::render();

    }
}